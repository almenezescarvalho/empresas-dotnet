﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class EnterprisesController : ApiController
    {

        public IEnumerable<Enterprise> Get()
        {
            List<Enterprise> empresas = new List<Enterprise>();

            try
            {
                SqlConnection objConexao = new SqlConnection(DatabaseSemEntity.ConexaoString);


                objConexao.Open();
                #region sql
                string strConn = @"SELECT e.[id]
      ,[email_enterprise]
      ,[facebook]
      ,[twitter]
      ,[linkedin]
      ,[phone]
      ,[own_enterprise]
      ,[enterprise_name]
      ,[photo]
      ,[description]
      ,[city]
      ,[country]
      ,[value]
      ,[share_price]
      ,[enterprise_type]
      ,t.enterprise_type_name
  FROM [ioasysdatabase].[dbo].[enterprises] e

 join enterprise_type t on e.enterprise_type =  t.id";
                #endregion
                SqlCommand objCommand = new SqlCommand(strConn, objConexao); // cmd string e conexão
                

                SqlDataReader dr = objCommand.ExecuteReader();

                Enterprise a = new Enterprise();

                while (dr.Read())
                {
                    
                    a.id = Convert.ToInt16(dr["id"] + "");
                    a.enterprise_name = dr["enterprise_name"] + "";
                    a.email_enterprise = dr["email_enterprise"] + "";
                    a.facebook = dr["facebook"] + "";
                    a.twitter = dr["twitter"] + "";
                    a.linkedin = dr["linkedin"] + "";
                    a.phone = dr["phone"] + "";
                    a.own_enterprise = Convert.ToBoolean(dr["own_enterprise"] + "");
                    a.photo = dr["photo"] + "";
                    a.description = dr["description"] + "";
                    a.city = dr["city"] + "";
                    a.country = dr["country"] + "";
                    a.value = Convert.ToDecimal(dr["value"] + "");
                    a.share_price = Convert.ToDecimal(dr["share_price"] + "");



                    a.enterprise_type = new Enterprise_type(Convert.ToInt16(dr["enterprise_type"] + ""), dr["enterprise_type_name"] + "");

                    empresas.Add(a); //adiciono a empresa
                    a = new Enterprise();  //limpo a instância para receber os dados da próxima empresa
                    

                }

             

                objConexao.Close();

                


            }
            catch (Exception)
            {
                //tratamento
                return null;
            }


            return empresas;
        }


        public IEnumerable<Enterprise> Get(int id)
        {
            List<Enterprise> empresas = new List<Enterprise>();

            try
            {
                SqlConnection objConexao = new SqlConnection(DatabaseSemEntity.ConexaoString);


                objConexao.Open();
                #region sql
                string strConn = @"SELECT e.[id]
      ,[email_enterprise]
      ,[facebook]
      ,[twitter]
      ,[linkedin]
      ,[phone]
      ,[own_enterprise]
      ,[enterprise_name]
      ,[photo]
      ,[description]
      ,[city]
      ,[country]
      ,[value]
      ,[share_price]
      ,[enterprise_type]
      ,t.enterprise_type_name
  FROM [ioasysdatabase].[dbo].[enterprises] e

 join enterprise_type t on e.enterprise_type =  t.id

where e.id = @id_param

";
                #endregion
                SqlCommand objCommand = new SqlCommand(strConn, objConexao); // cmd string e conexão
                objCommand.Parameters.AddWithValue("@id_param", id);

                SqlDataReader dr = objCommand.ExecuteReader();

                Enterprise a = new Enterprise();

                while (dr.Read())
                {

                    a.id = Convert.ToInt16(dr["id"] + "");
                    a.enterprise_name = dr["enterprise_name"] + "";
                    a.email_enterprise = dr["email_enterprise"] + "";
                    a.facebook = dr["facebook"] + "";
                    a.twitter = dr["twitter"] + "";
                    a.linkedin = dr["linkedin"] + "";
                    a.phone = dr["phone"] + "";
                    a.own_enterprise = Convert.ToBoolean(dr["own_enterprise"] + "");
                    a.photo = dr["photo"] + "";
                    a.description = dr["description"] + "";
                    a.city = dr["city"] + "";
                    a.country = dr["country"] + "";
                    a.value = Convert.ToDecimal(dr["value"] + "");
                    a.share_price = Convert.ToDecimal(dr["share_price"] + "");



                    a.enterprise_type = new Enterprise_type(Convert.ToInt16(dr["enterprise_type"] + ""), dr["enterprise_type_name"] + "");

                    empresas.Add(a); //adiciono a empresa
                    a = new Enterprise();  //limpo a instância para receber os dados da próxima empresa


                }



                objConexao.Close();




            }
            catch (Exception)
            {
                //tratamento
                return null;
            }


            return empresas;
        }


        //filtro tipo_empresa e nome_empresa
        public IEnumerable<Enterprise> Get(int enterprise_types, string name)
        {
            List<Enterprise> empresas = new List<Enterprise>();

            try
            {
                SqlConnection objConexao = new SqlConnection(DatabaseSemEntity.ConexaoString);


                objConexao.Open();
                #region sql
                string strConn = @"SELECT e.[id]
      ,[email_enterprise]
      ,[facebook]
      ,[twitter]
      ,[linkedin]
      ,[phone]
      ,[own_enterprise]
      ,[enterprise_name]
      ,[photo]
      ,[description]
      ,[city]
      ,[country]
      ,[value]
      ,[share_price]
      ,[enterprise_type]
      ,t.enterprise_type_name
  FROM [ioasysdatabase].[dbo].[enterprises] e

 join enterprise_type t on e.enterprise_type =  t.id

where e.enterprise_type = @enterprise_type_id or e.enterprise_name like @enterprise_name

";
                #endregion
                SqlCommand objCommand = new SqlCommand(strConn, objConexao); // cmd string e conexão
                objCommand.Parameters.AddWithValue("@enterprise_type_id", enterprise_types);
                objCommand.Parameters.AddWithValue("@enterprise_name", name);


                SqlDataReader dr = objCommand.ExecuteReader();

                Enterprise a = new Enterprise();

                while (dr.Read())
                {

                    a.id = Convert.ToInt16(dr["id"] + "");
                    a.enterprise_name = dr["enterprise_name"] + "";
                    a.email_enterprise = dr["email_enterprise"] + "";
                    a.facebook = dr["facebook"] + "";
                    a.twitter = dr["twitter"] + "";
                    a.linkedin = dr["linkedin"] + "";
                    a.phone = dr["phone"] + "";
                    a.own_enterprise = Convert.ToBoolean(dr["own_enterprise"] + "");
                    a.photo = dr["photo"] + "";
                    a.description = dr["description"] + "";
                    a.city = dr["city"] + "";
                    a.country = dr["country"] + "";
                    a.value = Convert.ToDecimal(dr["value"] + "");
                    a.share_price = Convert.ToDecimal(dr["share_price"] + "");



                    a.enterprise_type = new Enterprise_type(Convert.ToInt16(dr["enterprise_type"] + ""), dr["enterprise_type_name"] + "");

                    empresas.Add(a); //adiciono a empresa
                    a = new Enterprise();  //limpo a instância para receber os dados da próxima empresa


                }



                objConexao.Close();




            }
            catch (Exception)
            {
                //tratamento
                return null;
            }


            return empresas;
        }


    }
}
