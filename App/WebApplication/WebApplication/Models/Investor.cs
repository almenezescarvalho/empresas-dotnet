﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Investor
    {
        public int id { get; set; }

        public string investor_name { get; set; }

        public string email { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public decimal balance { get; set; }

        public string photo { get; set; }

        public Portfolio portfolio { get; set; }

        public decimal portfolio_value { get; set; }

        public bool first_access { get; set; }

        public bool super_angel { get; set; }


    }
}