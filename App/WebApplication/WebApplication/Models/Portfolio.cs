﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Portfolio
    {
        public Portfolio()
        {
            this.enterprises = new List<Enterprise>();
        }

        public int enterprises_number { get; set; }

        public List<Enterprise> enterprises { get; set; }

    }
}