-- SCRIPT DE CRIACAO DA BASE DE DADOS 
-- Gerado por Oracle SQL Developer Data Modeler 18.1.0.082.1035 
--   em:        2019-04-28 19:53:21 BRT
--   site:      SQL Server 2008
--   tipo:      SQL Server 2008



CREATE TABLE enterprise_type (
    id                     NUMERIC(28) NOT NULL,
    enterprise_type_name   VARCHAR(100) NOT NULL
)
   

ALTER TABLE enterprise_type ADD constraint enterprise_type_pk PRIMARY KEY CLUSTERED (id)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON )
    

CREATE TABLE enterprises (
    id                 NUMERIC(28) NOT NULL,
    email_enterprise   VARCHAR(100),
    facebook           VARCHAR(100),
    twitter            VARCHAR(100),
    linkedin           VARCHAR(100),
    phone              VARCHAR(20),
    own_enterprise     bit,
    enterprise_name    VARCHAR(50) NOT NULL,
    photo              VARCHAR(200),
    description        VARCHAR(900),
    city               VARCHAR(50),
    country            VARCHAR(50),
    value              NUMERIC(28),
    share_price        NUMERIC(15,2),
    enterprise_type    NUMERIC(28) NOT NULL
)
   

ALTER TABLE enterprises ADD constraint enterprises_pk PRIMARY KEY CLUSTERED (id)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON )
    


CREATE TABLE investor (
    id                NUMERIC(28) NOT NULL,
    password             VARCHAR(20) NOT NULL,
    investor_name     VARCHAR (50) NOT NULL,
    email             VARCHAR(100),
    city              VARCHAR(50),
    country           VARCHAR(50),
    balance           NUMERIC(15,2),
    photo             VARCHAR(600),
    portfolio_value   NUMERIC(15,2),
    first_access      bit,
    super_angel       bit
)
   

ALTER TABLE investor ADD constraint investor_pk PRIMARY KEY CLUSTERED (id)
     WITH (
     ALLOW_PAGE_LOCKS = ON , 
     ALLOW_ROW_LOCKS = ON )
    

CREATE TABLE portfolio (
    id_investor     NUMERIC(28) NOT NULL,
    id_enterprise   NUMERIC(28) NOT NULL
)
   

ALTER TABLE enterprises
    ADD CONSTRAINT enterprises_enterprise_type_fk FOREIGN KEY ( enterprise_type )
        REFERENCES enterprise_type ( id )


ALTER TABLE portfolio
    ADD CONSTRAINT portfolio_enterprises_fk FOREIGN KEY ( id_enterprise )
        REFERENCES enterprises ( id )


ALTER TABLE portfolio
    ADD CONSTRAINT portfolio_investor_fk FOREIGN KEY ( id_investor )
        REFERENCES investor ( id )




-- Relatório do Resumo do Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             4
-- CREATE INDEX                             0
-- ALTER TABLE                              6
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE DATABASE                          0
-- CREATE DEFAULT                           0
-- CREATE INDEX ON VIEW                     0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE ROLE                              0
-- CREATE RULE                              0
-- CREATE SCHEMA                            0
-- CREATE PARTITION FUNCTION                0
-- CREATE PARTITION SCHEME                  0
-- 
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
