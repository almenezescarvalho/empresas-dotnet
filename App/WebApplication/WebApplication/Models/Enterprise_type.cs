﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Enterprise_type
    {
        public Enterprise_type(int id, string enterprise_type_name)
        {
            this.id = id;
            this.enterprise_type_name = enterprise_type_name;
        }

        public int id { get; set; }

        public string enterprise_type_name { get; set; }
    }
}