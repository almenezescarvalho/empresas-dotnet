﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class Sign_InController : ApiController
    {
        public Investor Get(string email, string password)
        {
            List<Investor> investidor = new List<Investor>();

            try
            {
                SqlConnection objConexao = new SqlConnection(DatabaseSemEntity.ConexaoString);


                objConexao.Open();
                #region sql
                string strConn = @"SELECT [id]
      ,[password]
      ,[investor_name]
      ,[email]
      ,[city]
      ,[country]
      ,[balance]
      ,[photo]
      ,[portfolio_value]
      ,[first_access]
      ,[super_angel]
  FROM [ioasysdatabase].[dbo].[investor]
  
  where email = @email and password = @password";
                #endregion
                SqlCommand objCommand = new SqlCommand(strConn, objConexao); // cmd string e conexão
                objCommand.Parameters.AddWithValue("@email", email);
                objCommand.Parameters.AddWithValue("@password", password);


                SqlDataReader dr = objCommand.ExecuteReader();

                Investor i = new Investor();

                while (dr.Read())
                {

                    i.id = Convert.ToInt16(dr["id"] + "");
                    i.investor_name = dr["investor_name"] + "";
                    i.email = dr["email"] + "";
                    i.city = dr["city"] + "";
                    i.country = dr["country"] + "";
                    i.balance = Convert.ToDecimal(dr["balance"] + "");
                    i.photo = dr["photo"] + "";
                    i.portfolio_value = Convert.ToDecimal(dr["portfolio_value"] + "");
                    i.first_access = Convert.ToBoolean(dr["first_access"] + "");
                    i.super_angel = Convert.ToBoolean(dr["super_angel"] + "");





                    i.portfolio = new Portfolio();

                    return i;


                }



                objConexao.Close();




            }
            catch (Exception)
            {
                //tratamento
                return null;
            }


            return null;
        }



    }
}
